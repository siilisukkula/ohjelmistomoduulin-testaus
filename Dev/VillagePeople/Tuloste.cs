﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;


namespace VillagePeople
{
    public abstract class Tuloste
    {
        public const int INDENT_X = 60;
        public const int REGULAR_X = 30;
        public PointF currentPoint = new Point();
        public Font font = new Font("Arial", 16, FontStyle.Bold, GraphicsUnit.Point);

        public void printOtsikko(string otsikko, PrintPageEventArgs e) {
            RectangleF header = new RectangleF(30, 10, 600, 100);
            e.Graphics.DrawString(otsikko, font, Brushes.Black, header);
        }

        public void printAsiakasInfo(int id, PrintPageEventArgs e) {
            Asiakas a = Asiakas.Hae(id);

            List<string> rivit = new List<string>();
            rivit.Add("Asiakasnumero: " + a.Id);
            rivit.Add(a.Etunimi + " " + a.Sukunimi);
            rivit.Add(a.Lahiosoite);
            rivit.Add(a.Postinumero + " " + a.Postitoimipaikka);
            rivit.Add(a.Email);
            rivit.Add(a.Puhelinnumero);


            foreach (string rivi in rivit) {
                SizeF koko = e.Graphics.MeasureString(rivi, font);
                e.Graphics.DrawString(rivi, font, Brushes.Black, currentPoint);
                currentPoint.Y += koko.Height;
            }
        }

        public void printToimipisteInfo(int tpid, PrintPageEventArgs e) {
            Toimipiste toimipiste = Toimipiste.Hae(tpid);

            /* Toimipiste.Hae metodi antaa seuraavan virhekoodin: An exception of type 'System.Data.SQLite.SQLiteException' occurred in System.Data.SQLite.dll but was not handled in user code
            alla esimerkkitoimipiste testaamista varten*/
            /*
            Toimipiste toimipiste = Toimipiste.Hae(tpid);
            toimipiste.Nimi = "Esimerkki";
            toimipiste.Lahiosoite = "Esimerkkiosoite 5";
            toimipiste.Postinumero = "00000";
            toimipiste.Postitoimipaikka = "Esimerkkipostitoimipaikka";
            toimipiste.Email = "email@email.fi";
            toimipiste.Puhelinnumero = "0000000000";*/

            List<string> rivit = new List<string>();
            rivit.Add(toimipiste.Nimi);
            rivit.Add(toimipiste.Lahiosoite);
            rivit.Add(toimipiste.Postinumero + " " + toimipiste.Postitoimipaikka);
            rivit.Add(toimipiste.Email);
            rivit.Add(toimipiste.Puhelinnumero);


            foreach (string rivi in rivit) {
                SizeF koko = e.Graphics.MeasureString(rivi, font);
                e.Graphics.DrawString(rivi, font, Brushes.Black, currentPoint);
                currentPoint.Y += koko.Height;
            }
        }
    }
}