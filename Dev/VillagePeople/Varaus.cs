﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace VillagePeople
{
    public class Varaus : Entiteetti
    {
        int id;
        
        public Varaus()
        {
            VarausAlkuPvm = DateTime.Now;
            VarausLoppuPvm = DateTime.Now.AddDays(1);
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        Asiakas asiakas;

        public Asiakas Asiakas
        {
            get { return asiakas; }
            set { asiakas = value; }
        }
        DateTime varausPvm;

        public DateTime VarausPvm
        {
            get { return varausPvm; }
            set { varausPvm = value; }
        }
        DateTime vahvistusPvm;

        public DateTime VahvistusPvm
        {
            get { return vahvistusPvm; }
            set { vahvistusPvm = value; }
        }
        DateTime varausAlkuPvm;

        public DateTime VarausAlkuPvm
        {
            get { return varausAlkuPvm; }
            set { varausAlkuPvm = value; }
        }
        DateTime varausLoppuPvm;

        public DateTime VarausLoppuPvm
        {
            get { return varausLoppuPvm; }
            set { varausLoppuPvm = value; }
        }

        List<VarauksenPalvelu> palvelut = new List<VarauksenPalvelu>();
        public List<VarauksenPalvelu> Palvelut
        {
            get { return palvelut; }
            set { palvelut = value; }
        }

        public void Tallenna()
        {
            SQLiteConnection conn = new SQLiteConnection(Properties.Settings.Default.Database);
            conn.Open();
            SQLiteCommand comm = conn.CreateCommand();

            comm.CommandText = "INSERT OR REPLACE INTO varaus( id, a_id, varattu_pvm, vahvistus_pvm, varaus_alkupvm, varaus_loppupvm) VALUES " +
                "($id, $a_id, $varattu_pvm, $vahvistus_pvm, $varaus_alkupvm, $varaus_loppupvm)";

            comm.Parameters.AddWithValue("$id", Id);
            comm.Parameters.AddWithValue("$a_id", asiakas.Id);
            comm.Parameters.AddWithValue("$varattu_pvm", VarausPvm.ToString(Properties.Settings.Default.DateTimeFormat));
            comm.Parameters.AddWithValue("$vahvistus_pvm", VahvistusPvm.ToString(Properties.Settings.Default.DateTimeFormat));
            comm.Parameters.AddWithValue("$varaus_alkupvm", VarausAlkuPvm.ToString(Properties.Settings.Default.DateTimeFormat));
            comm.Parameters.AddWithValue("$varaus_loppupvm", VarausLoppuPvm.ToString(Properties.Settings.Default.DateTimeFormat));
         

            comm.ExecuteNonQuery();
            conn.Close();
            
            foreach( VarauksenPalvelu p in Palvelut)
            {
                p.Tallenna();
            }
        }

        public void Poista()
        {
            SQLiteConnection conn = new SQLiteConnection(Properties.Settings.Default.Database);
            conn.Open();
            SQLiteCommand comm = conn.CreateCommand();
            comm.CommandText = "DELETE FROM varaus WHERE id = $id";
            comm.Parameters.AddWithValue("$id", Id);
            comm.ExecuteNonQuery();
            conn.Close();
        }
        public static List<Varaus> HaeKaikki(Toimipiste t)
        {
            List<Varaus> varaukset = new List<Varaus>();
            DataSet ds = new DataSet();

            SQLiteConnection conn = new SQLiteConnection(Properties.Settings.Default.Database);
            conn.Open();
            SQLiteCommand comm = conn.CreateCommand();
            comm.CommandText = "SELECT v.id, v.a_id, v.varattu_pvm, v.vahvistus_pvm, v.varaus_alkupvm, v.varaus_loppupvm " +
                "FROM varaus v WHERE v.id IN "+
                "(SELECT distinct vp.v_id FROM varauksen_palvelut vp, palvelu p WHERE vp.p_id = p.id AND p.tp_id = $tp_id)";

            comm.Parameters.AddWithValue("$tp_id", t.Id);

            using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(comm))
            {
                adapter.Fill(ds);

                foreach( DataRow row in ds.Tables[0].Rows )
                {
                    Varaus varaus = new Varaus();

                    varaus.Id = int.Parse(row["id"].ToString());
                    varaus.VarausAlkuPvm = DateTime.Parse(row["varaus_alkupvm"].ToString());
                    varaus.VarausLoppuPvm = DateTime.Parse(row["varaus_loppupvm"].ToString());
                    varaus.VahvistusPvm = DateTime.Parse(row["varaus_loppupvm"].ToString());
                    varaus.VarausPvm = DateTime.Parse(row["varattu_pvm"].ToString());
                    varaus.Asiakas = Asiakas.Hae(int.Parse(row["a_id"].ToString()));
                    varaus.Palvelut = VarauksenPalvelu.HaePalvelut(varaus);
                    varaukset.Add(varaus);
                }

            }
            conn.Close();
            return varaukset;
        }

        public HashSet<DateTime> Paivat()
        {
            HashSet<DateTime> paivat = new HashSet<DateTime>();
            int kesto = (VarausLoppuPvm - VarausAlkuPvm).Days;

            for (int i = 0; i < kesto; i++)
            {
                paivat.Add(VarausAlkuPvm.AddDays(i));
            }
            return paivat;
        }

        public static Varaus Hae(int hakuId)
        {
            Varaus varaus = null;
            DataSet ds = new DataSet();

            SQLiteConnection conn = new SQLiteConnection(Properties.Settings.Default.Database);
            conn.Open();
            SQLiteCommand comm = conn.CreateCommand();
            comm.CommandText = "SELECT id, a_id, varattu_pvm, vahvistus_pvm, varaus_alkupvm, varaus_loppupvm "+
                "FROM varaus v WHERE id = $id";

            comm.Parameters.AddWithValue("$id", hakuId);

            using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(comm))
            {
                adapter.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    varaus = new Varaus();
                    varaus.Id = int.Parse(row["id"].ToString());
                    varaus.VarausAlkuPvm = DateTime.Parse(row["varaus_alkupvm"].ToString());
                    varaus.VarausLoppuPvm = DateTime.Parse(row["varaus_loppupvm"].ToString());
                    varaus.VahvistusPvm = DateTime.Parse(row["varaus_loppupvm"].ToString());
                    varaus.VarausPvm = DateTime.Parse(row["varattu_pvm"].ToString());
                    varaus.Asiakas = Asiakas.Hae(int.Parse(row["a_id"].ToString()));
                    varaus.Palvelut = VarauksenPalvelu.HaePalvelut(varaus);
                }

            }
            conn.Close();
            
            return varaus;
        }
        public override string ToString()
        {
            return Id + " " + VarausAlkuPvm + "-" + VarausLoppuPvm + " " + Asiakas.Etunimi + " " + Asiakas.Sukunimi;
        }
    }
}
/*CREATE TABLE varaus (
  id    INTEGER PRIMARY KEY,
  a_id  INTEGER,
  varattu_pvm TEXT,
  vahvistus_pvm TEXT,
  varaus_alkupvm TEXT,
  varaus_loppupvm TEXT,
  FOREIGN KEY (a_id) REFERENCES asiakas(id) ON DELETE CASCADE
)*/