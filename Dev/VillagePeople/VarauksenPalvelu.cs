﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace VillagePeople
{
    public class VarauksenPalvelu : Palvelu
    {
        int lukumaara;
        int palveluId;
        Varaus varaus;

        public VarauksenPalvelu(Varaus v)
        {
            varaus = v;
        }
        public void Aseta(Palvelu p)
        {
            PalveluId = p.Id;
            Hinta = p.Hinta;
            Kuvaus = p.Kuvaus;
            Nimi = p.Nimi;
            Toimipiste = p.Toimipiste;
            Alv = p.Alv;
        }
        public int PalveluId
        {
            get { return palveluId; }
            set { palveluId = value; }
        }

        public int Lukumaara
        {
            get { return lukumaara; }
            set { lukumaara = value; }
        }

        public new void Tallenna()
        {
            SQLiteConnection conn = new SQLiteConnection(Properties.Settings.Default.Database);
            conn.Open();
            SQLiteCommand comm = conn.CreateCommand();
            comm.CommandText = "INSERT OR REPLACE INTO varauksen_palvelut(id, p_id, v_id, lkm) VALUES ($id, $pid, $vid, $lkm)";
            comm.Parameters.AddWithValue("$id", Id);
            comm.Parameters.AddWithValue("$pid",PalveluId);
            comm.Parameters.AddWithValue("$vid", varaus.Id);
            comm.Parameters.AddWithValue("$lkm", Lukumaara);
            comm.ExecuteNonQuery();
            conn.Close();
        }

        public new void Poista()
        {
            SQLiteConnection conn = new SQLiteConnection(Properties.Settings.Default.Database);
            conn.Open();
            SQLiteCommand comm = conn.CreateCommand();
            comm.CommandText = "DELETE FROM varauksen_palvelut WHERE id = $id";
            comm.Parameters.AddWithValue("$id", Id);
            
            comm.ExecuteNonQuery();
            conn.Close();
        }

        static public List<VarauksenPalvelu> HaePalvelut(Varaus varaus)
        {
            List<VarauksenPalvelu> palvelut = new List<VarauksenPalvelu>();

            DataSet ds = new DataSet();

            SQLiteConnection conn = new SQLiteConnection(Properties.Settings.Default.Database);
            conn.Open();
            SQLiteCommand comm = conn.CreateCommand();
            comm.CommandText = "SELECT vp.id, p.id as palvelu_id, p.nimi, p.kuvaus, p.hinta, p.alv, vp.lkm " +
                "FROM varauksen_palvelut vp, palvelu p WHERE p.id = vp.p_id and vp.v_id = $vid;";
            comm.Parameters.AddWithValue("$vid", varaus.Id);

            using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(comm))
            {
                adapter.Fill(ds);
                foreach (DataRow row in ds.Tables[0].Rows)
                {

                    VarauksenPalvelu vp = new VarauksenPalvelu(varaus);
                    
                    vp.Id = int.Parse(row["id"].ToString());
                    vp.PalveluId = int.Parse(row["palvelu_id"].ToString());
                    vp.Nimi = row["nimi"].ToString();
                    vp.Kuvaus = row["kuvaus"].ToString();
                    vp.Hinta = double.Parse(row["hinta"].ToString());
                    vp.Alv = double.Parse(row["alv"].ToString());
                    vp.Lukumaara = int.Parse(row["lkm"].ToString());

                    palvelut.Add(vp);
                }

            }
            conn.Close();

            return palvelut;
        }
    }
}
