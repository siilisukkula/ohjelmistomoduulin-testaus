﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VillagePeople
{
    public class Lasku : Osoittellinen
    {
        int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        Varaus varaus;

        public Varaus Varaus
        {
            get { return varaus; }
            set { varaus = value; }
        }
        double summa;

        public double Summa
        {
            get { return summa; }
            set { summa = value; }
        }
        double alv;

        public double Alv
        {
            get { return alv; }
            set { alv = value; }
        }
    }
}
/*CREATE TABLE lasku (
  id    INTEGER PRIMARY KEY,
  v_id          INTEGER,
  nimi          TEXT,
  lahiosoite TEXT,
  postinro  TEXT,
  postitoimipaikka TEXT,
  summa REAL NOT NULL,
  alv   REAL NOT NULL,
  FOREIGN KEY (v_id) REFERENCES varaus(id) ON DELETE CASCADE
)*/