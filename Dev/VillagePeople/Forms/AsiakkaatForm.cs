﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace VillagePeople.Forms
{
    public partial class AsiakkaatForm : Form
    {
        public AsiakkaatForm()
        {
            InitializeComponent();
        }
        //Luodaan uusi asiakas ja tallennetaan kantaan
        private void uusiBtn_Click(object sender, EventArgs e)
        {
            string etunimi = textboxEtunimi.Text;
            string sukunimi = textboxSukunimi.Text;
            int id = Convert.ToInt32(textboxId.Text);
            string lahiosoite = textboxOsoite.Text;
            string postitoimipaikka = textboxPostitoim.Text;
            string postinumero = textboxPostinro.Text;
            string email = textboxEmail.Text;
            string puhnro = textboxPuhnro.Text;
            Save(etunimi, sukunimi, id, lahiosoite, postitoimipaikka, postinumero, email, puhnro);
            updateList();
            clearBoxes();
        }

        /*private void AsiakkaatForm_load(object sender, EventArgs e) {
        }*/
        
        //lisätään/tallennetaan asiakas
        private void Save(string enimi, string snimi, int id, string lahos, string postitoim, string postinro, string email, string puhnro)
        {
            Asiakas as1 = new Asiakas();

            as1.Etunimi = enimi;
            as1.Sukunimi = snimi;
            as1.Id = id;
            as1.Lahiosoite = lahos;
            as1.Postitoimipaikka = postitoim;
            as1.Postinumero = postinro;
            as1.Email = email;
            as1.Puhelinnumero = puhnro;

            as1.Tallenna();
        }

        //Poistetaan id:n määräämä asiakas
        private void Remove(int id) {
            Asiakas as1 = new Asiakas();
            as1.Id = id;
            as1.Poista();
        }
        //Päivitetään lista
        private void AsiakkaatForm_Load_1(object sender, EventArgs e)
        {
            updateList();

        }
        //Asetetaan tiedot kenttiin
        private void editBtn_Click(object sender, EventArgs e)
        {
            setInfo();
        }

        //päivitetään asiakkaatListv
        private void updateList() {
            List<Asiakas> asiakkaat = Asiakas.HaeKaikki();
            asiakkaatListv.Items.Clear();

            foreach (Asiakas a in asiakkaat)
            {
                asiakkaatListv.Items.Add(new ListViewItem(new[] {
                        a.Id.ToString(), a.Etunimi, a.Sukunimi, a.Lahiosoite, a.Postitoimipaikka, a.Postinumero, a.Email, a.Puhelinnumero
                }));
            }
        }

        //asetetaan textboxien sisällöksi valitun asiakkaan tiedot
        private void setInfo() {
            textboxId.Text = asiakkaatListv.SelectedItems[0].SubItems[0].Text;
            textboxEtunimi.Text = asiakkaatListv.SelectedItems[0].SubItems[1].Text;
            textboxSukunimi.Text = asiakkaatListv.SelectedItems[0].SubItems[2].Text;
            textboxOsoite.Text = asiakkaatListv.SelectedItems[0].SubItems[3].Text; ;
            textboxPostitoim.Text = asiakkaatListv.SelectedItems[0].SubItems[4].Text;
            textboxPostinro.Text = asiakkaatListv.SelectedItems[0].SubItems[5].Text;
            textboxEmail.Text = asiakkaatListv.SelectedItems[0].SubItems[6].Text;
            textboxPuhnro.Text = asiakkaatListv.SelectedItems[0].SubItems[7].Text;
        }

        //tyhjennetään textboxit
        private void clearBoxes() {
            textboxId.Text = null;
            textboxEtunimi.Text = null;
            textboxSukunimi.Text = null;
            textboxOsoite.Text = null;
            textboxPostitoim.Text = null;
            textboxPostinro.Text = null;
            textboxEmail.Text = null;
            textboxPuhnro.Text = null;
        }
        //Poistetaan listassa valittu asiakas
        private void poistaBtn_Click(object sender, EventArgs e)
        {
            Remove(Int32.Parse(asiakkaatListv.SelectedItems[0].Text));
            updateList();
        }
        //Luodaan alennus
        private void btnDiscount_Click(object sender, EventArgs e)
        {
            Alennus ale = new Alennus(tbAlennuskoodi.Text);
            if (chkboxNetto.Checked) { ale.Nettopaivat = true; }
            else { ale.Nettopaivat = false; }
            ale.AlennuspaivatPros1 = Double.Parse(tbAle2.Text);
            ale.AlennuspaivatPros2 = Double.Parse(tbAle2.Text);
        }

        private void enimiBox_TextChanged(object sender, EventArgs e)
        {
        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {
        }

        private void textboxId_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
