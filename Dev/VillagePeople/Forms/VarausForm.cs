﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Printing;
using System.Windows.Forms;

namespace VillagePeople.Forms
{
    public partial class VarausForm : Form
    {
        Varaus varaus;
        Toimipiste toimipiste;
        

        public Toimipiste Toimipiste
        {
            get { return toimipiste; }
            set { toimipiste = value; }
        }

        public VarausForm(Varaus v)
        {
            varaus = v;
            InitializeComponent();
            

        }

        private void lblAsiakas_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ( cbAsiakas.SelectedItem != null )
            {
                varaus.Asiakas = (Asiakas)cbAsiakas.SelectedItem;
            }
        }
        private void PaivitaAsiakkaat()
        {
            List<Asiakas> asiakkaat = Asiakas.HaeKaikki();
            cbAsiakas.Items.Clear();
            foreach (Asiakas a in asiakkaat)
            {
                cbAsiakas.Items.Add(a);
                if ( varaus.Asiakas != null )
                {
                    if (a.Id == varaus.Asiakas.Id) cbAsiakas.SelectedItem = a;
                }
            }
        }

        private void PaivitaVarauksenPalvelut()
        {
            lstVarauksenPalvelut.Items.Clear();
            
            foreach (VarauksenPalvelu vp in varaus.Palvelut)
            {
                lstVarauksenPalvelut.Items.Add(new ListViewItem(new[] { 
                        vp.Nimi,
                        vp.Lukumaara.ToString(),
                        vp.Hinta.ToString(),
                        (vp.Lukumaara * vp.Hinta).ToString(),
                        (vp.Lukumaara * vp.Hinta * vp.Alv).ToString()
                }));
            }
        
        }

        private void PaivitaPalvelut()
        {
            List<Palvelu> palvelut = Palvelu.HaeToimpisteella(toimipiste);
            // tyhjenna aiemmat
            lstPalvelut.Items.Clear();
            foreach (Palvelu p in palvelut)
            {
                // Luo alkio listviewiin
                ListViewItem item = new ListViewItem(new[]{
                    p.Id.ToString(),
                    p.Nimi,
                    p.Kuvaus,
                    p.Hinta.ToString(),
                    p.Alv.ToString()
                });

                lstPalvelut.Items.Add(item);
                // Teevarauksen päivien ja palvelun päivien leikkaus 
                HashSet<DateTime> leikkaus = p.HaeVaratutPaivat();
                leikkaus.IntersectWith(varaus.Paivat());
                
                // jos leikkaus ei ole tyhjä, palvelu on jo varattu varauksen aikavälille
                if (leikkaus.Count > 0) 
                {
                    item.ForeColor = Color.Red;
                } 
            }
        }

        private void VarausForm_Load(object sender, EventArgs e)
        {
            lblVarausNumero.Text = varaus.Id.ToString();
            PaivitaAsiakkaat();
            PaivitaVarauksenPalvelut();
            PaivitaPalvelut();
            datetimeVarausAlku.Value = varaus.VarausAlkuPvm;
            datetimeVarausLoppu.Value = varaus.VarausLoppuPvm;

            if (varaus.VahvistusPvm != DateTime.MinValue)
            {
                cbVahvistettu.Checked = true;
                
                datetimeVahvistettuPvm.Value = varaus.VahvistusPvm;
            }
            else
            {
                cbVahvistettu.Checked = false;
                datetimeVahvistettuPvm.Enabled = false;
            }
        }

        private void lblAsiakkaantiedot_Click(object sender, EventArgs e)
        {

        }

        private void lstVarauksenPalvelut_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnLisaa_Click(object sender, EventArgs e)
        {
            int palveluId = int.Parse(lstPalvelut.SelectedItems[0].SubItems[0].Text);
            VarauksenPalvelu p = new VarauksenPalvelu(varaus);
            p.Aseta(Palvelu.Hae(palveluId));
            varaus.Palvelut.Add(p);
            varaus.Tallenna();

            PaivitaVarauksenPalvelut();
        }

        private void btnPoista_Click(object sender, EventArgs e)
        {

        }

        private void lstPalvelut_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool onkoValittu = (lstPalvelut.SelectedItems.Count > 0 );
            btnLisaa.Enabled = onkoValittu;
            
            if ( onkoValittu ) 
            {
                int palveluId = int.Parse(lstPalvelut.SelectedItems[0].SubItems[0].Text);
                calVaraustilanne.BoldedDates = Palvelu.Hae(palveluId).HaeVaratutPaivat().ToArray();
            }
        }

        private void btnTilaus_Click(object sender, EventArgs e)
        {
            tulostaEsikatselu.Document = tilausVahvistus;
            tulostaEsikatselu.ShowDialog();
        }

        

        private void tilausVahvistus_PrintPage(object sender, PrintPageEventArgs e)
        {
            TilVahvTuloste tilv = new TilVahvTuloste(varaus.Asiakas.Id, toimipiste.Id,
                                  varaus.VarausAlkuPvm.ToString("dd.MM."), varaus.VarausLoppuPvm.ToString("dd.MM.yyyy"));
            tilv.Print(e);
        }

        private void laskuDokumentti_PrintPage(object sender, PrintPageEventArgs e)
        {
            LaskuTuloste lsk = new LaskuTuloste(varaus.Asiakas.Id, toimipiste.Id, 300.25, varaus.VarausAlkuPvm.ToString("dd.MM.yyyy"));
            lsk.Print(e);
        }

        private void btnLasku_Click(object sender, EventArgs e)
        {
            tulostaEsikatselu.Document = laskuDokumentti;
            tulostaEsikatselu.ShowDialog();
        }
    }
}
