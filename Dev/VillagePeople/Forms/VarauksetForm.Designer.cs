﻿namespace VillagePeople
{
    partial class VarauksetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbToimipiste = new System.Windows.Forms.ComboBox();
            this.lblToimipiste = new System.Windows.Forms.Label();
            this.datetimeRajausAlku = new System.Windows.Forms.DateTimePicker();
            this.datetimeRajausLoppu = new System.Windows.Forms.DateTimePicker();
            this.lblAlkupvm = new System.Windows.Forms.Label();
            this.lblLoppupvm = new System.Windows.Forms.Label();
            this.lstVaraukset = new System.Windows.Forms.ListView();
            this.Varausnumero = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Alkupvm = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.loppupvm = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AsiakasEtunimi = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AsiakasSukunimi = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnUusiVaraus = new System.Windows.Forms.Button();
            this.btnToimipisteet = new System.Windows.Forms.Button();
            this.btnAsiakkaat = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbToimipiste
            // 
            this.cbToimipiste.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbToimipiste.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbToimipiste.FormattingEnabled = true;
            this.cbToimipiste.Location = new System.Drawing.Point(21, 64);
            this.cbToimipiste.Name = "cbToimipiste";
            this.cbToimipiste.Size = new System.Drawing.Size(98, 21);
            this.cbToimipiste.TabIndex = 0;
            this.cbToimipiste.SelectedIndexChanged += new System.EventHandler(this.cbToimipiste_SelectedIndexChanged);
            // 
            // lblToimipiste
            // 
            this.lblToimipiste.AutoSize = true;
            this.lblToimipiste.Location = new System.Drawing.Point(18, 48);
            this.lblToimipiste.Name = "lblToimipiste";
            this.lblToimipiste.Size = new System.Drawing.Size(54, 13);
            this.lblToimipiste.TabIndex = 1;
            this.lblToimipiste.Text = "Toimipiste";
            // 
            // datetimeRajausAlku
            // 
            this.datetimeRajausAlku.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datetimeRajausAlku.Location = new System.Drawing.Point(125, 65);
            this.datetimeRajausAlku.Name = "datetimeRajausAlku";
            this.datetimeRajausAlku.Size = new System.Drawing.Size(104, 20);
            this.datetimeRajausAlku.TabIndex = 2;
            this.datetimeRajausAlku.ValueChanged += new System.EventHandler(this.datetimeRajausAlku_ValueChanged);
            // 
            // datetimeRajausLoppu
            // 
            this.datetimeRajausLoppu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datetimeRajausLoppu.Location = new System.Drawing.Point(235, 64);
            this.datetimeRajausLoppu.Name = "datetimeRajausLoppu";
            this.datetimeRajausLoppu.Size = new System.Drawing.Size(101, 20);
            this.datetimeRajausLoppu.TabIndex = 3;
            this.datetimeRajausLoppu.ValueChanged += new System.EventHandler(this.datetimeRajausLoppu_ValueChanged);
            // 
            // lblAlkupvm
            // 
            this.lblAlkupvm.AutoSize = true;
            this.lblAlkupvm.Location = new System.Drawing.Point(122, 48);
            this.lblAlkupvm.Name = "lblAlkupvm";
            this.lblAlkupvm.Size = new System.Drawing.Size(54, 13);
            this.lblAlkupvm.TabIndex = 4;
            this.lblAlkupvm.Text = "Alkupäivä";
            // 
            // lblLoppupvm
            // 
            this.lblLoppupvm.AutoSize = true;
            this.lblLoppupvm.Location = new System.Drawing.Point(232, 48);
            this.lblLoppupvm.Name = "lblLoppupvm";
            this.lblLoppupvm.Size = new System.Drawing.Size(63, 13);
            this.lblLoppupvm.TabIndex = 5;
            this.lblLoppupvm.Text = "Loppupäivä";
            // 
            // lstVaraukset
            // 
            this.lstVaraukset.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Varausnumero,
            this.Alkupvm,
            this.loppupvm,
            this.AsiakasEtunimi,
            this.AsiakasSukunimi});
            this.lstVaraukset.FullRowSelect = true;
            this.lstVaraukset.HideSelection = false;
            this.lstVaraukset.Location = new System.Drawing.Point(21, 91);
            this.lstVaraukset.Name = "lstVaraukset";
            this.lstVaraukset.Size = new System.Drawing.Size(631, 272);
            this.lstVaraukset.TabIndex = 9;
            this.lstVaraukset.UseCompatibleStateImageBehavior = false;
            this.lstVaraukset.View = System.Windows.Forms.View.Details;
            this.lstVaraukset.SelectedIndexChanged += new System.EventHandler(this.lstVaraukset_SelectedIndexChanged);
            // 
            // Varausnumero
            // 
            this.Varausnumero.Text = "Varausnumero";
            this.Varausnumero.Width = 98;
            // 
            // Alkupvm
            // 
            this.Alkupvm.Text = "Alkupvm";
            this.Alkupvm.Width = 78;
            // 
            // loppupvm
            // 
            this.loppupvm.Text = "Loppupvm";
            this.loppupvm.Width = 78;
            // 
            // AsiakasEtunimi
            // 
            this.AsiakasEtunimi.Text = "Asiakkaan etunimi";
            this.AsiakasEtunimi.Width = 115;
            // 
            // AsiakasSukunimi
            // 
            this.AsiakasSukunimi.Text = "Asiakaaan sukunimi";
            this.AsiakasSukunimi.Width = 141;
            // 
            // btnUusiVaraus
            // 
            this.btnUusiVaraus.Location = new System.Drawing.Point(577, 64);
            this.btnUusiVaraus.Name = "btnUusiVaraus";
            this.btnUusiVaraus.Size = new System.Drawing.Size(75, 23);
            this.btnUusiVaraus.TabIndex = 10;
            this.btnUusiVaraus.Text = "Uusi varaus";
            this.btnUusiVaraus.UseVisualStyleBackColor = true;
            this.btnUusiVaraus.Click += new System.EventHandler(this.btnUusiVaraus_Click);
            // 
            // btnToimipisteet
            // 
            this.btnToimipisteet.Location = new System.Drawing.Point(21, 13);
            this.btnToimipisteet.Name = "btnToimipisteet";
            this.btnToimipisteet.Size = new System.Drawing.Size(75, 23);
            this.btnToimipisteet.TabIndex = 11;
            this.btnToimipisteet.Text = "Toimpisteet ja palvelut";
            this.btnToimipisteet.UseVisualStyleBackColor = true;
            // 
            // btnAsiakkaat
            // 
            this.btnAsiakkaat.Location = new System.Drawing.Point(577, 13);
            this.btnAsiakkaat.Name = "btnAsiakkaat";
            this.btnAsiakkaat.Size = new System.Drawing.Size(75, 23);
            this.btnAsiakkaat.TabIndex = 13;
            this.btnAsiakkaat.Text = "Asiakkaat";
            this.btnAsiakkaat.UseVisualStyleBackColor = true;
            this.btnAsiakkaat.Click += new System.EventHandler(this.btnAsiakkaat_Click);
            // 
            // VarauksetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(669, 375);
            this.Controls.Add(this.btnAsiakkaat);
            this.Controls.Add(this.btnToimipisteet);
            this.Controls.Add(this.btnUusiVaraus);
            this.Controls.Add(this.lstVaraukset);
            this.Controls.Add(this.lblLoppupvm);
            this.Controls.Add(this.lblAlkupvm);
            this.Controls.Add(this.datetimeRajausLoppu);
            this.Controls.Add(this.datetimeRajausAlku);
            this.Controls.Add(this.lblToimipiste);
            this.Controls.Add(this.cbToimipiste);
            this.Name = "VarauksetForm";
            this.Text = "Varaukset";
            this.Load += new System.EventHandler(this.VarausForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbToimipiste;
        private System.Windows.Forms.Label lblToimipiste;
        private System.Windows.Forms.DateTimePicker datetimeRajausAlku;
        private System.Windows.Forms.DateTimePicker datetimeRajausLoppu;
        private System.Windows.Forms.Label lblAlkupvm;
        private System.Windows.Forms.Label lblLoppupvm;
        private System.Windows.Forms.ListView lstVaraukset;
        private System.Windows.Forms.ColumnHeader Varausnumero;
        private System.Windows.Forms.ColumnHeader Alkupvm;
        private System.Windows.Forms.ColumnHeader loppupvm;
        private System.Windows.Forms.ColumnHeader AsiakasEtunimi;
        private System.Windows.Forms.ColumnHeader AsiakasSukunimi;
        private System.Windows.Forms.Button btnUusiVaraus;
        private System.Windows.Forms.Button btnToimipisteet;
        private System.Windows.Forms.Button btnAsiakkaat;
    }
}

