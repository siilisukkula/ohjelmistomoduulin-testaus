﻿namespace VillagePeople.Forms
{
    partial class AsiakkaatForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uusiBtn = new System.Windows.Forms.Button();
            this.asiakkaatListv = new System.Windows.Forms.ListView();
            this.ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Etunimi = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Sukunimi = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Lähiosoite = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Postitoimipaikka = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Postinumero = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Sähköposti = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Puhelinnro = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.poistaBtn = new System.Windows.Forms.Button();
            this.textboxEtunimi = new System.Windows.Forms.TextBox();
            this.lblEtunimi = new System.Windows.Forms.Label();
            this.lblSukunimi = new System.Windows.Forms.Label();
            this.textboxSukunimi = new System.Windows.Forms.TextBox();
            this.lblPosti = new System.Windows.Forms.Label();
            this.textboxPostitoim = new System.Windows.Forms.TextBox();
            this.lblOsoite = new System.Windows.Forms.Label();
            this.textboxOsoite = new System.Windows.Forms.TextBox();
            this.lblPuhnro = new System.Windows.Forms.Label();
            this.textboxPuhnro = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.textboxEmail = new System.Windows.Forms.TextBox();
            this.lblpostinro = new System.Windows.Forms.Label();
            this.textboxPostinro = new System.Windows.Forms.TextBox();
            this.lblId = new System.Windows.Forms.Label();
            this.textboxId = new System.Windows.Forms.TextBox();
            this.editBtn = new System.Windows.Forms.Button();
            this.tbAlennuskoodi = new System.Windows.Forms.TextBox();
            this.tbAle1 = new System.Windows.Forms.TextBox();
            this.tbAle2 = new System.Windows.Forms.TextBox();
            this.lblAle = new System.Windows.Forms.Label();
            this.lblAle1 = new System.Windows.Forms.Label();
            this.lblAle2 = new System.Windows.Forms.Label();
            this.btnDiscount = new System.Windows.Forms.Button();
            this.chkboxNetto = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // uusiBtn
            // 
            this.uusiBtn.Location = new System.Drawing.Point(214, 499);
            this.uusiBtn.Name = "uusiBtn";
            this.uusiBtn.Size = new System.Drawing.Size(118, 23);
            this.uusiBtn.TabIndex = 3;
            this.uusiBtn.Text = "Save";
            this.uusiBtn.UseVisualStyleBackColor = true;
            this.uusiBtn.Click += new System.EventHandler(this.uusiBtn_Click);
            // 
            // asiakkaatListv
            // 
            this.asiakkaatListv.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ID,
            this.Etunimi,
            this.Sukunimi,
            this.Lähiosoite,
            this.Postitoimipaikka,
            this.Postinumero,
            this.Sähköposti,
            this.Puhelinnro});
            this.asiakkaatListv.FullRowSelect = true;
            this.asiakkaatListv.HideSelection = false;
            this.asiakkaatListv.Location = new System.Drawing.Point(12, 12);
            this.asiakkaatListv.Name = "asiakkaatListv";
            this.asiakkaatListv.Size = new System.Drawing.Size(620, 253);
            this.asiakkaatListv.TabIndex = 4;
            this.asiakkaatListv.UseCompatibleStateImageBehavior = false;
            this.asiakkaatListv.View = System.Windows.Forms.View.Details;
            // 
            // ID
            // 
            this.ID.Text = "ID";
            // 
            // Etunimi
            // 
            this.Etunimi.Text = "Etunimi";
            // 
            // Sukunimi
            // 
            this.Sukunimi.Text = "Sukunimi";
            // 
            // Lähiosoite
            // 
            this.Lähiosoite.Text = "Lähiosoite";
            this.Lähiosoite.Width = 86;
            // 
            // Postitoimipaikka
            // 
            this.Postitoimipaikka.Text = "Postitoimipaikka";
            this.Postitoimipaikka.Width = 122;
            // 
            // Postinumero
            // 
            this.Postinumero.Text = "Postinumero";
            // 
            // Sähköposti
            // 
            this.Sähköposti.Text = "Sähköposti";
            // 
            // Puhelinnro
            // 
            this.Puhelinnro.Text = "Puhelinnro";
            // 
            // poistaBtn
            // 
            this.poistaBtn.Location = new System.Drawing.Point(557, 271);
            this.poistaBtn.Name = "poistaBtn";
            this.poistaBtn.Size = new System.Drawing.Size(75, 23);
            this.poistaBtn.TabIndex = 5;
            this.poistaBtn.Text = "Delete";
            this.poistaBtn.UseVisualStyleBackColor = true;
            this.poistaBtn.Click += new System.EventHandler(this.poistaBtn_Click);
            // 
            // textboxEtunimi
            // 
            this.textboxEtunimi.Location = new System.Drawing.Point(117, 327);
            this.textboxEtunimi.Name = "textboxEtunimi";
            this.textboxEtunimi.Size = new System.Drawing.Size(215, 20);
            this.textboxEtunimi.TabIndex = 6;
            this.textboxEtunimi.TextChanged += new System.EventHandler(this.enimiBox_TextChanged);
            // 
            // lblEtunimi
            // 
            this.lblEtunimi.AutoSize = true;
            this.lblEtunimi.Location = new System.Drawing.Point(25, 327);
            this.lblEtunimi.Name = "lblEtunimi";
            this.lblEtunimi.Size = new System.Drawing.Size(44, 13);
            this.lblEtunimi.TabIndex = 7;
            this.lblEtunimi.Text = "Etunimi:";
            // 
            // lblSukunimi
            // 
            this.lblSukunimi.AutoSize = true;
            this.lblSukunimi.Location = new System.Drawing.Point(25, 353);
            this.lblSukunimi.Name = "lblSukunimi";
            this.lblSukunimi.Size = new System.Drawing.Size(53, 13);
            this.lblSukunimi.TabIndex = 9;
            this.lblSukunimi.Text = "Sukunimi:";
            // 
            // textboxSukunimi
            // 
            this.textboxSukunimi.Location = new System.Drawing.Point(117, 353);
            this.textboxSukunimi.Name = "textboxSukunimi";
            this.textboxSukunimi.Size = new System.Drawing.Size(215, 20);
            this.textboxSukunimi.TabIndex = 8;
            // 
            // lblPosti
            // 
            this.lblPosti.AutoSize = true;
            this.lblPosti.Location = new System.Drawing.Point(25, 402);
            this.lblPosti.Name = "lblPosti";
            this.lblPosti.Size = new System.Drawing.Size(86, 13);
            this.lblPosti.TabIndex = 13;
            this.lblPosti.Text = "Postitoimipaikka:";
            // 
            // textboxPostitoim
            // 
            this.textboxPostitoim.Location = new System.Drawing.Point(117, 399);
            this.textboxPostitoim.Name = "textboxPostitoim";
            this.textboxPostitoim.Size = new System.Drawing.Size(215, 20);
            this.textboxPostitoim.TabIndex = 12;
            this.textboxPostitoim.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // lblOsoite
            // 
            this.lblOsoite.AutoSize = true;
            this.lblOsoite.Location = new System.Drawing.Point(25, 376);
            this.lblOsoite.Name = "lblOsoite";
            this.lblOsoite.Size = new System.Drawing.Size(58, 13);
            this.lblOsoite.TabIndex = 11;
            this.lblOsoite.Text = "Lähiosoite:";
            // 
            // textboxOsoite
            // 
            this.textboxOsoite.Location = new System.Drawing.Point(117, 376);
            this.textboxOsoite.Name = "textboxOsoite";
            this.textboxOsoite.Size = new System.Drawing.Size(215, 20);
            this.textboxOsoite.TabIndex = 10;
            // 
            // lblPuhnro
            // 
            this.lblPuhnro.AutoSize = true;
            this.lblPuhnro.Location = new System.Drawing.Point(25, 476);
            this.lblPuhnro.Name = "lblPuhnro";
            this.lblPuhnro.Size = new System.Drawing.Size(80, 13);
            this.lblPuhnro.TabIndex = 19;
            this.lblPuhnro.Text = "Puhelinnumero:";
            // 
            // textboxPuhnro
            // 
            this.textboxPuhnro.Location = new System.Drawing.Point(117, 473);
            this.textboxPuhnro.Name = "textboxPuhnro";
            this.textboxPuhnro.Size = new System.Drawing.Size(215, 20);
            this.textboxPuhnro.TabIndex = 18;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(25, 450);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(63, 13);
            this.lblEmail.TabIndex = 17;
            this.lblEmail.Text = "Sähköposti:";
            // 
            // textboxEmail
            // 
            this.textboxEmail.Location = new System.Drawing.Point(117, 450);
            this.textboxEmail.Name = "textboxEmail";
            this.textboxEmail.Size = new System.Drawing.Size(215, 20);
            this.textboxEmail.TabIndex = 16;
            // 
            // lblpostinro
            // 
            this.lblpostinro.AutoSize = true;
            this.lblpostinro.Location = new System.Drawing.Point(25, 427);
            this.lblpostinro.Name = "lblpostinro";
            this.lblpostinro.Size = new System.Drawing.Size(68, 13);
            this.lblpostinro.TabIndex = 15;
            this.lblpostinro.Text = "Postinumero:";
            // 
            // textboxPostinro
            // 
            this.textboxPostinro.Location = new System.Drawing.Point(117, 424);
            this.textboxPostinro.Name = "textboxPostinro";
            this.textboxPostinro.Size = new System.Drawing.Size(215, 20);
            this.textboxPostinro.TabIndex = 14;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(25, 301);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(21, 13);
            this.lblId.TabIndex = 21;
            this.lblId.Text = "ID:";
            // 
            // textboxId
            // 
            this.textboxId.Location = new System.Drawing.Point(117, 301);
            this.textboxId.Name = "textboxId";
            this.textboxId.Size = new System.Drawing.Size(215, 20);
            this.textboxId.TabIndex = 20;
            this.textboxId.TextChanged += new System.EventHandler(this.textboxId_TextChanged);
            // 
            // editBtn
            // 
            this.editBtn.Location = new System.Drawing.Point(476, 271);
            this.editBtn.Name = "editBtn";
            this.editBtn.Size = new System.Drawing.Size(75, 23);
            this.editBtn.TabIndex = 22;
            this.editBtn.Text = "Edit";
            this.editBtn.UseVisualStyleBackColor = true;
            this.editBtn.Click += new System.EventHandler(this.editBtn_Click);
            // 
            // tbAlennuskoodi
            // 
            this.tbAlennuskoodi.Location = new System.Drawing.Point(492, 327);
            this.tbAlennuskoodi.Name = "tbAlennuskoodi";
            this.tbAlennuskoodi.Size = new System.Drawing.Size(100, 20);
            this.tbAlennuskoodi.TabIndex = 24;
            // 
            // tbAle1
            // 
            this.tbAle1.Location = new System.Drawing.Point(492, 350);
            this.tbAle1.Name = "tbAle1";
            this.tbAle1.Size = new System.Drawing.Size(100, 20);
            this.tbAle1.TabIndex = 26;
            this.tbAle1.Text = "0";
            this.tbAle1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbAle2
            // 
            this.tbAle2.Location = new System.Drawing.Point(492, 376);
            this.tbAle2.Name = "tbAle2";
            this.tbAle2.Size = new System.Drawing.Size(100, 20);
            this.tbAle2.TabIndex = 27;
            this.tbAle2.Text = "0";
            this.tbAle2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblAle
            // 
            this.lblAle.AutoSize = true;
            this.lblAle.Location = new System.Drawing.Point(395, 330);
            this.lblAle.Name = "lblAle";
            this.lblAle.Size = new System.Drawing.Size(74, 13);
            this.lblAle.TabIndex = 28;
            this.lblAle.Text = "Alennuskoodi:";
            // 
            // lblAle1
            // 
            this.lblAle1.AutoSize = true;
            this.lblAle1.Location = new System.Drawing.Point(395, 357);
            this.lblAle1.Name = "lblAle1";
            this.lblAle1.Size = new System.Drawing.Size(86, 13);
            this.lblAle1.TabIndex = 30;
            this.lblAle1.Text = "Alennuspäivät 1:";
            // 
            // lblAle2
            // 
            this.lblAle2.AutoSize = true;
            this.lblAle2.Location = new System.Drawing.Point(395, 383);
            this.lblAle2.Name = "lblAle2";
            this.lblAle2.Size = new System.Drawing.Size(86, 13);
            this.lblAle2.TabIndex = 31;
            this.lblAle2.Text = "Alennuspäivät 2:";
            // 
            // btnDiscount
            // 
            this.btnDiscount.Location = new System.Drawing.Point(492, 450);
            this.btnDiscount.Name = "btnDiscount";
            this.btnDiscount.Size = new System.Drawing.Size(100, 23);
            this.btnDiscount.TabIndex = 32;
            this.btnDiscount.Text = "Create discount";
            this.btnDiscount.UseVisualStyleBackColor = true;
            this.btnDiscount.Click += new System.EventHandler(this.btnDiscount_Click);
            // 
            // chkboxNetto
            // 
            this.chkboxNetto.AutoSize = true;
            this.chkboxNetto.Location = new System.Drawing.Point(503, 417);
            this.chkboxNetto.Name = "chkboxNetto";
            this.chkboxNetto.Size = new System.Drawing.Size(81, 17);
            this.chkboxNetto.TabIndex = 33;
            this.chkboxNetto.Text = "Nettopäivät";
            this.chkboxNetto.UseVisualStyleBackColor = true;
            // 
            // AsiakkaatForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 546);
            this.Controls.Add(this.chkboxNetto);
            this.Controls.Add(this.btnDiscount);
            this.Controls.Add(this.lblAle2);
            this.Controls.Add(this.lblAle1);
            this.Controls.Add(this.lblAle);
            this.Controls.Add(this.tbAle2);
            this.Controls.Add(this.tbAle1);
            this.Controls.Add(this.tbAlennuskoodi);
            this.Controls.Add(this.editBtn);
            this.Controls.Add(this.lblId);
            this.Controls.Add(this.textboxId);
            this.Controls.Add(this.lblPuhnro);
            this.Controls.Add(this.textboxPuhnro);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.textboxEmail);
            this.Controls.Add(this.lblpostinro);
            this.Controls.Add(this.textboxPostinro);
            this.Controls.Add(this.lblPosti);
            this.Controls.Add(this.textboxPostitoim);
            this.Controls.Add(this.lblOsoite);
            this.Controls.Add(this.textboxOsoite);
            this.Controls.Add(this.lblSukunimi);
            this.Controls.Add(this.textboxSukunimi);
            this.Controls.Add(this.lblEtunimi);
            this.Controls.Add(this.textboxEtunimi);
            this.Controls.Add(this.poistaBtn);
            this.Controls.Add(this.asiakkaatListv);
            this.Controls.Add(this.uusiBtn);
            this.Name = "AsiakkaatForm";
            this.Text = "Asiakkaat";
            this.Load += new System.EventHandler(this.AsiakkaatForm_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button uusiBtn;
        private System.Windows.Forms.ListView asiakkaatListv;
        private System.Windows.Forms.Button poistaBtn;
        private System.Windows.Forms.ColumnHeader ID;
        private System.Windows.Forms.ColumnHeader Etunimi;
        private System.Windows.Forms.ColumnHeader Sukunimi;
        private System.Windows.Forms.TextBox textboxEtunimi;
        private System.Windows.Forms.Label lblEtunimi;
        private System.Windows.Forms.Label lblSukunimi;
        private System.Windows.Forms.TextBox textboxSukunimi;
        private System.Windows.Forms.Label lblPosti;
        private System.Windows.Forms.TextBox textboxPostitoim;
        private System.Windows.Forms.Label lblOsoite;
        private System.Windows.Forms.TextBox textboxOsoite;
        private System.Windows.Forms.Label lblPuhnro;
        private System.Windows.Forms.TextBox textboxPuhnro;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox textboxEmail;
        private System.Windows.Forms.Label lblpostinro;
        private System.Windows.Forms.TextBox textboxPostinro;
        private System.Windows.Forms.ColumnHeader Lähiosoite;
        private System.Windows.Forms.ColumnHeader Postitoimipaikka;
        private System.Windows.Forms.ColumnHeader Postinumero;
        private System.Windows.Forms.ColumnHeader Sähköposti;
        private System.Windows.Forms.ColumnHeader Puhelinnro;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.TextBox textboxId;
        private System.Windows.Forms.Button editBtn;
        private System.Windows.Forms.TextBox tbAlennuskoodi;
        private System.Windows.Forms.TextBox tbAle1;
        private System.Windows.Forms.TextBox tbAle2;
        private System.Windows.Forms.Label lblAle;
        private System.Windows.Forms.Label lblAle1;
        private System.Windows.Forms.Label lblAle2;
        private System.Windows.Forms.Button btnDiscount;
        private System.Windows.Forms.CheckBox chkboxNetto;
    }
}