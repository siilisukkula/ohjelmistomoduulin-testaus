﻿namespace VillagePeople.Forms
{
    partial class VarausForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VarausForm));
            this.cbAsiakas = new System.Windows.Forms.ComboBox();
            this.lblAsiakas = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblVarausNroText = new System.Windows.Forms.Label();
            this.btnLisaa = new System.Windows.Forms.Button();
            this.btnPoista = new System.Windows.Forms.Button();
            this.datetimeVarausAlku = new System.Windows.Forms.DateTimePicker();
            this.datetimeVarausLoppu = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.lblHinta = new System.Windows.Forms.Label();
            this.lblVarausNumero = new System.Windows.Forms.Label();
            this.lstVarauksenPalvelut = new System.Windows.Forms.ListView();
            this.Palvelunimi = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Lukumaara = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Hinta = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Alv = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnAsiakasUusi = new System.Windows.Forms.Button();
            this.datetimeVahvistettuPvm = new System.Windows.Forms.DateTimePicker();
            this.cbVahvistettu = new System.Windows.Forms.CheckBox();
            this.lstPalvelut = new System.Windows.Forms.ListView();
            this.itempalvelu = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.kuvaus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.txtbPalveluHaku = new System.Windows.Forms.TextBox();
            this.calVaraustilanne = new System.Windows.Forms.MonthCalendar();
            this.lblVaraustilanne = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnLasku = new System.Windows.Forms.Button();
            this.btnTilaus = new System.Windows.Forms.Button();
            this.tilausVahvistus = new System.Drawing.Printing.PrintDocument();
            this.tulostaEsikatselu = new System.Windows.Forms.PrintPreviewDialog();
            this.laskuDokumentti = new System.Drawing.Printing.PrintDocument();
            this.SuspendLayout();
            // 
            // cbAsiakas
            // 
            this.cbAsiakas.FormattingEnabled = true;
            this.cbAsiakas.Location = new System.Drawing.Point(341, 12);
            this.cbAsiakas.Name = "cbAsiakas";
            this.cbAsiakas.Size = new System.Drawing.Size(160, 21);
            this.cbAsiakas.TabIndex = 0;
            this.cbAsiakas.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // lblAsiakas
            // 
            this.lblAsiakas.AutoSize = true;
            this.lblAsiakas.Location = new System.Drawing.Point(291, 15);
            this.lblAsiakas.Name = "lblAsiakas";
            this.lblAsiakas.Size = new System.Drawing.Size(44, 13);
            this.lblAsiakas.TabIndex = 1;
            this.lblAsiakas.Text = "Asiakas";
            this.lblAsiakas.Click += new System.EventHandler(this.lblAsiakas_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 132);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Varauksen palvelut";
            // 
            // lblVarausNroText
            // 
            this.lblVarausNroText.AutoSize = true;
            this.lblVarausNroText.Location = new System.Drawing.Point(13, 15);
            this.lblVarausNroText.Name = "lblVarausNroText";
            this.lblVarausNroText.Size = new System.Drawing.Size(78, 13);
            this.lblVarausNroText.TabIndex = 4;
            this.lblVarausNroText.Text = "Varausnumero:";
            this.lblVarausNroText.Click += new System.EventHandler(this.lblAsiakkaantiedot_Click);
            // 
            // btnLisaa
            // 
            this.btnLisaa.Location = new System.Drawing.Point(260, 216);
            this.btnLisaa.Name = "btnLisaa";
            this.btnLisaa.Size = new System.Drawing.Size(27, 23);
            this.btnLisaa.TabIndex = 5;
            this.btnLisaa.Text = "+";
            this.btnLisaa.UseVisualStyleBackColor = true;
            this.btnLisaa.Click += new System.EventHandler(this.btnLisaa_Click);
            // 
            // btnPoista
            // 
            this.btnPoista.Location = new System.Drawing.Point(260, 168);
            this.btnPoista.Name = "btnPoista";
            this.btnPoista.Size = new System.Drawing.Size(27, 23);
            this.btnPoista.TabIndex = 6;
            this.btnPoista.Text = "-";
            this.btnPoista.UseVisualStyleBackColor = true;
            this.btnPoista.Click += new System.EventHandler(this.btnPoista_Click);
            // 
            // datetimeVarausAlku
            // 
            this.datetimeVarausAlku.Location = new System.Drawing.Point(13, 62);
            this.datetimeVarausAlku.Name = "datetimeVarausAlku";
            this.datetimeVarausAlku.Size = new System.Drawing.Size(143, 20);
            this.datetimeVarausAlku.TabIndex = 7;
            // 
            // datetimeVarausLoppu
            // 
            this.datetimeVarausLoppu.Location = new System.Drawing.Point(162, 62);
            this.datetimeVarausLoppu.Name = "datetimeVarausLoppu";
            this.datetimeVarausLoppu.Size = new System.Drawing.Size(140, 20);
            this.datetimeVarausLoppu.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Varauksen kesto";
            // 
            // lblHinta
            // 
            this.lblHinta.AutoSize = true;
            this.lblHinta.Location = new System.Drawing.Point(12, 461);
            this.lblHinta.Name = "lblHinta";
            this.lblHinta.Size = new System.Drawing.Size(77, 13);
            this.lblHinta.TabIndex = 10;
            this.lblHinta.Text = "Kokonaishinta:";
            // 
            // lblVarausNumero
            // 
            this.lblVarausNumero.AutoSize = true;
            this.lblVarausNumero.Location = new System.Drawing.Point(113, 15);
            this.lblVarausNumero.Name = "lblVarausNumero";
            this.lblVarausNumero.Size = new System.Drawing.Size(96, 13);
            this.lblVarausNumero.TabIndex = 11;
            this.lblVarausNumero.Text = "Varauksen numero";
            // 
            // lstVarauksenPalvelut
            // 
            this.lstVarauksenPalvelut.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Palvelunimi,
            this.Lukumaara,
            this.Hinta,
            this.Alv});
            this.lstVarauksenPalvelut.FullRowSelect = true;
            this.lstVarauksenPalvelut.LabelEdit = true;
            this.lstVarauksenPalvelut.Location = new System.Drawing.Point(15, 152);
            this.lstVarauksenPalvelut.Name = "lstVarauksenPalvelut";
            this.lstVarauksenPalvelut.Size = new System.Drawing.Size(241, 300);
            this.lstVarauksenPalvelut.TabIndex = 13;
            this.lstVarauksenPalvelut.UseCompatibleStateImageBehavior = false;
            this.lstVarauksenPalvelut.View = System.Windows.Forms.View.Details;
            this.lstVarauksenPalvelut.SelectedIndexChanged += new System.EventHandler(this.lstVarauksenPalvelut_SelectedIndexChanged);
            // 
            // Palvelunimi
            // 
            this.Palvelunimi.Text = "Palvelu";
            // 
            // Lukumaara
            // 
            this.Lukumaara.Text = "Lukumäärä";
            this.Lukumaara.Width = 68;
            // 
            // Hinta
            // 
            this.Hinta.Text = "Hinta";
            this.Hinta.Width = 41;
            // 
            // Alv
            // 
            this.Alv.Text = "Alv:n osuus";
            // 
            // btnAsiakasUusi
            // 
            this.btnAsiakasUusi.Location = new System.Drawing.Point(507, 10);
            this.btnAsiakasUusi.Name = "btnAsiakasUusi";
            this.btnAsiakasUusi.Size = new System.Drawing.Size(75, 23);
            this.btnAsiakasUusi.TabIndex = 14;
            this.btnAsiakasUusi.Text = "Uusi asiakas";
            this.btnAsiakasUusi.UseVisualStyleBackColor = true;
            // 
            // datetimeVahvistettuPvm
            // 
            this.datetimeVahvistettuPvm.Location = new System.Drawing.Point(101, 88);
            this.datetimeVahvistettuPvm.Name = "datetimeVahvistettuPvm";
            this.datetimeVahvistettuPvm.Size = new System.Drawing.Size(100, 20);
            this.datetimeVahvistettuPvm.TabIndex = 15;
            // 
            // cbVahvistettu
            // 
            this.cbVahvistettu.AutoSize = true;
            this.cbVahvistettu.Location = new System.Drawing.Point(15, 91);
            this.cbVahvistettu.Name = "cbVahvistettu";
            this.cbVahvistettu.Size = new System.Drawing.Size(79, 17);
            this.cbVahvistettu.TabIndex = 17;
            this.cbVahvistettu.Text = "Vahvistettu";
            this.cbVahvistettu.UseVisualStyleBackColor = true;
            // 
            // lstPalvelut
            // 
            this.lstPalvelut.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lstPalvelut.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.itempalvelu,
            this.kuvaus});
            this.lstPalvelut.FullRowSelect = true;
            this.lstPalvelut.HideSelection = false;
            this.lstPalvelut.LabelEdit = true;
            this.lstPalvelut.Location = new System.Drawing.Point(294, 152);
            this.lstPalvelut.MultiSelect = false;
            this.lstPalvelut.Name = "lstPalvelut";
            this.lstPalvelut.Size = new System.Drawing.Size(262, 300);
            this.lstPalvelut.TabIndex = 18;
            this.lstPalvelut.UseCompatibleStateImageBehavior = false;
            this.lstPalvelut.View = System.Windows.Forms.View.Details;
            this.lstPalvelut.SelectedIndexChanged += new System.EventHandler(this.lstPalvelut_SelectedIndexChanged);
            // 
            // itempalvelu
            // 
            this.itempalvelu.Text = "Nimi";
            this.itempalvelu.Width = 101;
            // 
            // kuvaus
            // 
            this.kuvaus.Text = "Kuvaus";
            this.kuvaus.Width = 151;
            // 
            // txtbPalveluHaku
            // 
            this.txtbPalveluHaku.Location = new System.Drawing.Point(294, 129);
            this.txtbPalveluHaku.Name = "txtbPalveluHaku";
            this.txtbPalveluHaku.Size = new System.Drawing.Size(100, 20);
            this.txtbPalveluHaku.TabIndex = 19;
            // 
            // calVaraustilanne
            // 
            this.calVaraustilanne.Location = new System.Drawing.Point(568, 152);
            this.calVaraustilanne.Name = "calVaraustilanne";
            this.calVaraustilanne.TabIndex = 21;
            // 
            // lblVaraustilanne
            // 
            this.lblVaraustilanne.AutoSize = true;
            this.lblVaraustilanne.Location = new System.Drawing.Point(565, 136);
            this.lblVaraustilanne.Name = "lblVaraustilanne";
            this.lblVaraustilanne.Size = new System.Drawing.Size(114, 13);
            this.lblVaraustilanne.TabIndex = 22;
            this.lblVaraustilanne.Text = "Palvelun varaustilanne";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(401, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Valittavat palvelut";
            // 
            // btnLasku
            // 
            this.btnLasku.Location = new System.Drawing.Point(462, 63);
            this.btnLasku.Name = "btnLasku";
            this.btnLasku.Size = new System.Drawing.Size(94, 23);
            this.btnLasku.TabIndex = 24;
            this.btnLasku.Text = "Lasku";
            this.btnLasku.UseVisualStyleBackColor = true;
            this.btnLasku.Click += new System.EventHandler(this.btnLasku_Click);
            // 
            // btnTilaus
            // 
            this.btnTilaus.Location = new System.Drawing.Point(308, 62);
            this.btnTilaus.Name = "btnTilaus";
            this.btnTilaus.Size = new System.Drawing.Size(94, 23);
            this.btnTilaus.TabIndex = 25;
            this.btnTilaus.Text = "Tilausvahvistus";
            this.btnTilaus.UseVisualStyleBackColor = true;
            this.btnTilaus.Click += new System.EventHandler(this.btnTilaus_Click);
            // 
            // tilausVahvistus
            // 
            this.tilausVahvistus.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.tilausVahvistus_PrintPage);
            // 
            // tulostaEsikatselu
            // 
            this.tulostaEsikatselu.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.tulostaEsikatselu.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.tulostaEsikatselu.ClientSize = new System.Drawing.Size(400, 300);
            this.tulostaEsikatselu.Enabled = true;
            this.tulostaEsikatselu.Icon = ((System.Drawing.Icon)(resources.GetObject("tulostaEsikatselu.Icon")));
            this.tulostaEsikatselu.Name = "printPreviewDialog1";
            this.tulostaEsikatselu.Visible = false;
            // 
            // laskuDokumentti
            // 
            this.laskuDokumentti.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.laskuDokumentti_PrintPage);
            // 
            // VarausForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(760, 479);
            this.Controls.Add(this.btnTilaus);
            this.Controls.Add(this.btnLasku);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblVaraustilanne);
            this.Controls.Add(this.calVaraustilanne);
            this.Controls.Add(this.txtbPalveluHaku);
            this.Controls.Add(this.lstPalvelut);
            this.Controls.Add(this.cbVahvistettu);
            this.Controls.Add(this.datetimeVahvistettuPvm);
            this.Controls.Add(this.btnAsiakasUusi);
            this.Controls.Add(this.lstVarauksenPalvelut);
            this.Controls.Add(this.lblVarausNumero);
            this.Controls.Add(this.lblHinta);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.datetimeVarausLoppu);
            this.Controls.Add(this.datetimeVarausAlku);
            this.Controls.Add(this.btnPoista);
            this.Controls.Add(this.btnLisaa);
            this.Controls.Add(this.lblVarausNroText);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblAsiakas);
            this.Controls.Add(this.cbAsiakas);
            this.Name = "VarausForm";
            this.Text = "VarausForm";
            this.Load += new System.EventHandler(this.VarausForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbAsiakas;
        private System.Windows.Forms.Label lblAsiakas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblVarausNroText;
        private System.Windows.Forms.Button btnLisaa;
        private System.Windows.Forms.Button btnPoista;
        private System.Windows.Forms.DateTimePicker datetimeVarausAlku;
        private System.Windows.Forms.DateTimePicker datetimeVarausLoppu;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblHinta;
        private System.Windows.Forms.Label lblVarausNumero;
        private System.Windows.Forms.ListView lstVarauksenPalvelut;
        private System.Windows.Forms.ColumnHeader Palvelunimi;
        private System.Windows.Forms.ColumnHeader Lukumaara;
        private System.Windows.Forms.ColumnHeader Hinta;
        private System.Windows.Forms.ColumnHeader Alv;
        private System.Windows.Forms.Button btnAsiakasUusi;
        private System.Windows.Forms.DateTimePicker datetimeVahvistettuPvm;
        private System.Windows.Forms.CheckBox cbVahvistettu;
        private System.Windows.Forms.ListView lstPalvelut;
        private System.Windows.Forms.TextBox txtbPalveluHaku;
        private System.Windows.Forms.ColumnHeader itempalvelu;
        private System.Windows.Forms.ColumnHeader kuvaus;
        private System.Windows.Forms.MonthCalendar calVaraustilanne;
        private System.Windows.Forms.Label lblVaraustilanne;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnLasku;
        private System.Windows.Forms.Button btnTilaus;
        private System.Drawing.Printing.PrintDocument tilausVahvistus;
        private System.Windows.Forms.PrintPreviewDialog tulostaEsikatselu;
        private System.Drawing.Printing.PrintDocument laskuDokumentti;
    }
}