﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VillagePeople.Forms;

namespace VillagePeople
{
    public partial class VarauksetForm : Form
    {
        public VarauksetForm()
        {
            InitializeComponent();
        }

        private void cbToimipiste_SelectedIndexChanged(object sender, EventArgs e)
        {
            PaivitaVaraukset();
        }

        private void VarausForm_Load(object sender, EventArgs e)
        {
            
            datetimeRajausAlku.Value        = DateTime.Now;
            datetimeRajausLoppu.Value       = DateTime.Now.AddDays(30.0);
            
            List<Toimipiste> toimipisteet   = Toimipiste.HaeKaikki();
            cbToimipiste.Items.AddRange(toimipisteet.ToArray());
            cbToimipiste.SelectedItem = toimipisteet[0];
        }

        private void datetimeRajausAlku_ValueChanged(object sender, EventArgs e)
        {
            PaivitaVaraukset();
        }

        private void PaivitaVaraukset()
        {

            lstVaraukset.Items.Clear();
            Toimipiste tp = (Toimipiste)cbToimipiste.SelectedItem;
            if (tp != null)
            {
                List<Varaus> varaukset = Varaus.HaeKaikki(tp);
                foreach (Varaus v in varaukset)
                {
                    bool alkuPvmAikaValilla = (v.VarausAlkuPvm >= datetimeRajausAlku.Value && v.VarausAlkuPvm <= datetimeRajausLoppu.Value);
                    bool loppuPvmAikaValilla = (v.VarausLoppuPvm >= datetimeRajausAlku.Value && v.VarausLoppuPvm <= datetimeRajausLoppu.Value);

                    if (alkuPvmAikaValilla || loppuPvmAikaValilla)
                    {
                        lstVaraukset.Items.Add(new ListViewItem(new []{ v.Id.ToString(), 
                                                                        v.VarausAlkuPvm.ToString(), 
                                                                        v.VarausLoppuPvm.ToString(), 
                                                                        v.Asiakas.Etunimi, 
                                                                        v.Asiakas.Sukunimi}));
                    }
                }

            }
        }

        private void datetimeRajausLoppu_ValueChanged(object sender, EventArgs e)
        {
            PaivitaVaraukset();
        }

        private void lstVaraukset_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstVaraukset.SelectedItems.Count > 0)
            {
                int varausId = int.Parse(lstVaraukset.SelectedItems[0].SubItems[0].Text);
                Varaus v = Varaus.Hae(varausId);
                VarausForm form = new VarausForm(v);
                Toimipiste tp = (Toimipiste)cbToimipiste.SelectedItem;
                form.Toimipiste = tp;
                form.ShowDialog();
                
            }
        }

        private void btnUusiVaraus_Click(object sender, EventArgs e)
        {
            Varaus v = new Varaus();
            VarausForm form = new VarausForm(v);
            Toimipiste tp = (Toimipiste)cbToimipiste.SelectedItem;
            form.Toimipiste = tp;
            form.ShowDialog();
        }

        private void btnAsiakkaat_Click(object sender, EventArgs e)
        {
            AsiakkaatForm form = new AsiakkaatForm();
            form.ShowDialog();
        }
    }
}
