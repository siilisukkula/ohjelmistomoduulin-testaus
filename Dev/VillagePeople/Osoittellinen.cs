﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace VillagePeople
{
    public class Osoittellinen
    {
        string lahiosoite;

        public string Lahiosoite
        {
            get { return lahiosoite; }
            set { lahiosoite = value; }
        }
        string postitoimipaikka;

        public string Postitoimipaikka
        {
            get { return postitoimipaikka; }
            set { postitoimipaikka = value; }
        }
        string postinumero;

        public string Postinumero
        {
            get { return postinumero; }
            set { postinumero = value; }
        }
        string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        string puhelinnumero;

        public string Puhelinnumero
        {
            get { return puhelinnumero; }
            set { puhelinnumero = value; }
        }
        protected void ParsiOsoite(DataRow row)
        {
            lahiosoite = row["lahiosoite"].ToString();
            postitoimipaikka = row["postitoimipaikka"].ToString();
            postinumero = row["postinro"].ToString();
            email = row["email"].ToString();
            puhelinnumero = row["puhelinnro"].ToString();
        }
        protected void AsetaOsoitteenArvot(SQLiteParameterCollection parameters)
        {
            parameters.AddWithValue("$lahiosoite", Lahiosoite);
            parameters.AddWithValue("$postitoimipaikka", Postitoimipaikka);
            parameters.AddWithValue("$postinro", Postinumero);
            parameters.AddWithValue("$email", Email);
            parameters.AddWithValue("$puhelinnro", Puhelinnumero);
        }
    }
}
