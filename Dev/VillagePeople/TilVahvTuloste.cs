﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Printing;
using System.Windows.Forms;

namespace VillagePeople
{
    public class TilVahvTuloste : Tuloste
    {

        int asid, tpid;
        string VarausAlkuPvm, VarausLoppuPvm;

        public TilVahvTuloste(int asid, int tpid, string VarausAlkuPvm, string VarausLoppuPvm)
        {
            this.asid = asid;
            this.tpid = tpid;
            this.VarausAlkuPvm = VarausAlkuPvm;
            this.VarausLoppuPvm = VarausLoppuPvm;
        }

        public void Print(PrintPageEventArgs e) {
            printOtsikko("Village People Oy                          TILAUSVAHVISTUS", e);
            e.Graphics.DrawLine(new Pen(Brushes.Black), new Point(REGULAR_X, 100), new Point(600, 100));
            currentPoint.X = INDENT_X;
            currentPoint.Y = 150;
            printAsiakasInfo(asid, e);
            currentPoint.X = REGULAR_X;
            printValiotsikko(e);
            printToimipisteInfo(tpid, e);
        }

        private void printValiotsikko(PrintPageEventArgs e) {
            string valiOtsikko = "Teille varatut palvelut: " + VarausAlkuPvm + "-" + VarausLoppuPvm;
            SizeF koko = e.Graphics.MeasureString(valiOtsikko, font);
            e.Graphics.DrawString(valiOtsikko, font, Brushes.Black, currentPoint);
            currentPoint.Y += koko.Height;
            e.Graphics.DrawLine(new Pen(Brushes.Black), new Point(30, (int)currentPoint.Y), new Point(600, (int)currentPoint.Y));
            valiOtsikko = "Vahvistus varaukselle tehtävä " + VarausAlkuPvm + " mennessä toimipisteeseen :";
            koko = e.Graphics.MeasureString(valiOtsikko, font);
            currentPoint.Y += koko.Height;
            e.Graphics.DrawString(valiOtsikko, font, Brushes.Black, currentPoint);
            currentPoint.Y += koko.Height * 2;
        }
    }
}
            