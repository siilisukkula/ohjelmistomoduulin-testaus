﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Printing;
using System.Windows.Forms;

namespace VillagePeople
{
    class LaskuTuloste : Tuloste
    {
        int asid, tpid;
        double summa;
        string pvm;
        public LaskuTuloste(int asid, int tpid, double summa, string pvm) {
            this.asid = asid;
            this.tpid = tpid;
            this.summa = summa;
            this.pvm = pvm;
        }

        public void Print(PrintPageEventArgs e)
        {
            printOtsikko("Village People Oy                          LASKUTULOSTE", e);
            e.Graphics.DrawLine(new Pen(Brushes.Black), new Point(REGULAR_X, 100), new Point(600, 100));
            currentPoint.X = INDENT_X;
            currentPoint.Y = 150;
            printAsiakasInfo(asid, e);
            currentPoint.X = REGULAR_X;
            printLaskutiedot(e);
            printTuotteet(e);
            printSumma(e);
            //printToimipisteInfo(e);
        }

        private void printLaskutiedot(PrintPageEventArgs e) {
            string laskuTiedot = "Olette ostaneet seuraavat palvelut / tuotteet: ";
            SizeF koko = e.Graphics.MeasureString(laskuTiedot, font);
            e.Graphics.DrawString(laskuTiedot, font, Brushes.Black, currentPoint);
            currentPoint.Y += koko.Height;
        }

        private void printTuotteet(PrintPageEventArgs e) {
            List<string> ostetut = new List<string>();
            ostetut.Add("Esimerkki 1");
            ostetut.Add("Esimerkki 2");
            ostetut.Add("Esimerkki 3");

            foreach(string x in ostetut){
                SizeF koko = e.Graphics.MeasureString(x, font);
                e.Graphics.DrawString(x, font, Brushes.Black, currentPoint);
                currentPoint.Y += koko.Height;
            }
        }

        private void printSumma(PrintPageEventArgs e) {
            string Summa = "Summa: " + summa + "€";
            SizeF koko = e.Graphics.MeasureString(Summa, font);
            e.Graphics.DrawString(Summa, font, Brushes.Black, currentPoint);
            currentPoint.Y += koko.Height;
        }
    }
}
