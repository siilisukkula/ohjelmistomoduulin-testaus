﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;

namespace VillagePeople
{
    public class Asiakas : Osoittellinen, Entiteetti
    {
        int id;
        string etunimi;
        
        public string Etunimi
        {
            get { return etunimi; }
            set { etunimi = value; }
        }
        string sukunimi;

        public string Sukunimi
        {
            get { return sukunimi; }
            set { sukunimi = value; }
        }
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public static Asiakas Hae(int hakuId)
        {
            Asiakas asiakas = null;
            DataSet ds = new DataSet();
            
            SQLiteConnection conn = new SQLiteConnection(Properties.Settings.Default.Database);
            conn.Open();
            SQLiteCommand comm = conn.CreateCommand();
            comm.CommandText = "SELECT id, etunimi, sukunimi, lahiosoite, postitoimipaikka, postinro, email, puhelinnro " +
                "FROM asiakas WHERE id = $id";
            comm.Parameters.AddWithValue("$id", hakuId);

            using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(comm))            
            {
                adapter.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    asiakas = new Asiakas();
                    DataRow row = ds.Tables[0].Rows[0];
                    asiakas.Id = int.Parse(row["id"].ToString());
                    asiakas.Etunimi = row["etunimi"].ToString();
                    asiakas.Sukunimi = row["sukunimi"].ToString();

                    asiakas.ParsiOsoite(row);
                }
                
            }
            conn.Close();
            return asiakas;
        }
        public static List<Asiakas> HaeKaikki()
        {
            List<Asiakas> asiakkaat = new List<Asiakas>();
            DataSet ds = new DataSet();

            SQLiteConnection conn = new SQLiteConnection(Properties.Settings.Default.Database);
            conn.Open();
            SQLiteCommand comm = conn.CreateCommand();
            comm.CommandText = "SELECT id, etunimi, sukunimi, lahiosoite, postitoimipaikka, postinro, email, puhelinnro FROM asiakas";
            
            using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(comm))
            {
                adapter.Fill(ds);
                foreach( DataRow row in ds.Tables[0].Rows)
                {
                    Asiakas asiakas = new Asiakas();
                    
                    asiakas.Id = int.Parse(row["id"].ToString());
                    asiakas.Etunimi = row["etunimi"].ToString();
                    asiakas.Sukunimi = row["sukunimi"].ToString();
                    asiakas.ParsiOsoite(row);

                    asiakkaat.Add(asiakas);
                }

            }
            conn.Close();
            return asiakkaat;
        }
        public void Tallenna()
        {
            SQLiteConnection conn = new SQLiteConnection(Properties.Settings.Default.Database);
            conn.Open();
            SQLiteCommand comm = conn.CreateCommand();
            comm.CommandText = "INSERT OR REPLACE INTO asiakas(id, etunimi, sukunimi, lahiosoite, postitoimipaikka, postinro, email, puhelinnro) " +
                "VALUES ($id, $etunimi, $sukunimi, $lahiosoite, $postitoimipaikka, $postinro, $email, $puhelinnro)";

            comm.Parameters.AddWithValue("$id", this.Id);
            comm.Parameters.AddWithValue("$etunimi", this.Etunimi);
            comm.Parameters.AddWithValue("$sukunimi", this.Sukunimi);
            comm.Parameters.AddWithValue("$lahiosoite", this.Lahiosoite);
            comm.Parameters.AddWithValue("$postitoimipaikka", this.Postitoimipaikka);
            comm.Parameters.AddWithValue("$postinro", this.Postinumero);
            comm.Parameters.AddWithValue("$email", this.Email);
            comm.Parameters.AddWithValue("$puhelinnro", this.Puhelinnumero);
            comm.ExecuteNonQuery();
            
            conn.Close();
        }

        public void Poista()
        { 
            SQLiteConnection conn = new SQLiteConnection(Properties.Settings.Default.Database);
            conn.Open();
            SQLiteCommand comm = conn.CreateCommand();
            comm.CommandText = "DELETE FROM asiakas WHERE id = $id";

            comm.Parameters.AddWithValue("$id", this.Id);
            comm.ExecuteNonQuery();
            
            conn.Close();
        }
        public override string ToString()
        {
            return Etunimi + " " + Sukunimi; 
        }
    }
}
/*
 * CREATE TABLE asiakas (
  id  INTEGER PRIMARY KEY,
  etunimi TEXT,
  sukunimi TEXT,
  lahiosoite TEXT,
  postitoimipaikka TEXT,
  postinro  TEXT,
  email TEXT,
  puhelinnro TEXT
)

*/