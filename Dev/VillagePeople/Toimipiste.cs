﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
namespace VillagePeople
{
    public class Toimipiste : Osoittellinen, Entiteetti
    {
        int id;
        List<Palvelu> palvelut;

        public List<Palvelu> Palvelut
        {
            get { return palvelut; }
            set { palvelut = value; }
        }
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        string nimi;

        public string Nimi
        {
            get { return nimi; }
            set { nimi = value; }
        }

        public void Tallenna()
        {

            SQLiteConnection conn = new SQLiteConnection(Properties.Settings.Default.Database);
            conn.Open();
            SQLiteCommand comm = conn.CreateCommand();
            comm.CommandText = "INSERT OR REPLANCE INTO toimipiste(id, nimi, lahiosoite, postitoimipaikka, postinro, email, puhelinnro) "+
                " VALUES($id, $nimi, $lahiosoite, $postitoimipaikka, $postinro, $email, $puhelinnro)";
            comm.Parameters.AddWithValue("$id", Id);
            comm.Parameters.AddWithValue("$nimi", Nimi);
            
            AsetaOsoitteenArvot(comm.Parameters);
            comm.ExecuteNonQuery();
            
            conn.Close();
        }
        
        public void Poista()
        {
            SQLiteConnection conn = new SQLiteConnection(Properties.Settings.Default.Database);
            SQLiteCommand comm = conn.CreateCommand();
            comm.CommandText = "DELETE FROM toimipiste WHERE id=$id";
            comm.Parameters.AddWithValue("$id", Id);
            comm.ExecuteNonQuery();

            conn.Close();
        }
        public override string ToString()
        {
            return Nimi;
        }
        public static List<Toimipiste> HaeKaikki()
        {
            List<Toimipiste> toimipisteet = new List<Toimipiste>();
            DataSet ds = new DataSet();
            
            SQLiteConnection conn = new SQLiteConnection(Properties.Settings.Default.Database);
            conn.Open();
            SQLiteCommand comm = conn.CreateCommand();
            comm.CommandText = "SELECT  id, nimi, lahiosoite, postitoimipaikka, postinro, email, puhelinnro FROM toimipiste";
            
            using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(comm.CommandText,conn))
            {
                adapter.Fill(ds);

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Toimipiste toimipiste = new Toimipiste();

                    toimipiste.Id = int.Parse(row["id"].ToString());
                    toimipiste.Nimi = row["nimi"].ToString();
                    toimipiste.ParsiOsoite(row);
                    
                    toimipisteet.Add(toimipiste);
                }

            }
            conn.Close();

            return toimipisteet;
        }

        public static Toimipiste Hae(int toimipisteId)
        {
            Toimipiste toimipiste = null;
            DataSet ds = new DataSet();

            SQLiteConnection conn = new SQLiteConnection(Properties.Settings.Default.Database);
            conn.Open();
            SQLiteCommand comm = conn.CreateCommand();
            comm.CommandText = "SELECT  id, nimi, lahiosoite, postitoimipaikka, postinro, email, puhelinnro FROM toimipiste WHERE id = $id";
            comm.Parameters.AddWithValue("$id", toimipisteId);

            using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(comm.CommandText, conn))
            {
                adapter.Fill(ds);

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    toimipiste = new Toimipiste();

                    toimipiste.Id = int.Parse(row["id"].ToString());
                    toimipiste.Nimi = row["nimi"].ToString();
                    toimipiste.ParsiOsoite(row);

                }

            }
            conn.Close();

            return toimipiste;
        }
    }
}
/*CREATE TABLE toimipiste (
  id  INTEGER PRIMARY KEY,
  nimi TEXT,
  lahiosoite TEXT,
  postitoimipaikka TEXT,
  postinro  TEXT,
  email TEXT,
  puhelinnro TEXT
)*/