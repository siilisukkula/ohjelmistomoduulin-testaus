﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;

namespace VillagePeople
{
    public class Palvelu : Entiteetti
    {
        int id;
        Toimipiste toimipiste;

        public Toimipiste Toimipiste
        {
            get { return toimipiste; }
            set { toimipiste = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        string nimi;

        public string Nimi
        {
            get { return nimi; }
            set { nimi = value; }
        }
        string kuvaus;

        public string Kuvaus
        {
            get { return kuvaus; }
            set { kuvaus = value; }
        }
        double alv;

        public double Alv
        {
            get { return alv; }
            set { alv = value; }
        }
        double hinta;
        public double Hinta
        {
            get { return hinta; }
            set { hinta = value; }
        }

        public void Tallenna()
        { 
            
        }

        public void Poista()
        { 
            
        }

        static public List<Palvelu> HaeToimpisteella(Toimipiste toimipiste)
        {
            List<Palvelu> palvelut = new List<Palvelu>();

            DataSet ds = new DataSet();

            SQLiteConnection conn = new SQLiteConnection(Properties.Settings.Default.Database);
            conn.Open();
            SQLiteCommand comm = conn.CreateCommand();
            comm.CommandText = "SELECT id, nimi, kuvaus, hinta, alv FROM palvelu WHERE tp_id = $tpid";
            
            comm.Parameters.AddWithValue("$tpid", toimipiste.Id);

            using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(comm))
            {
                adapter.Fill(ds);
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Palvelu p = new Palvelu();

                    p.Id = int.Parse(row["id"].ToString());
                    p.Nimi = row["nimi"].ToString();
                    p.Kuvaus = row["kuvaus"].ToString();
                    p.Hinta = double.Parse(row["hinta"].ToString());
                    p.Alv = double.Parse(row["alv"].ToString());
                    p.Toimipiste = toimipiste;
                    palvelut.Add(p);
                }

            }
            conn.Close();

            return palvelut;
        }

        public HashSet<DateTime> HaeVaratutPaivat()
        {
            HashSet<DateTime> varatutPaivat = new HashSet<DateTime>();
            
            DataSet ds = new DataSet();

            SQLiteConnection conn = new SQLiteConnection(Properties.Settings.Default.Database);
            conn.Open();
            SQLiteCommand comm = conn.CreateCommand();
            comm.CommandText = "SELECT vp.p_id, v.varaus_alkupvm, v.varaus_loppupvm " +
                                "FROM varauksen_palvelut vp, varaus v " +
                                "WHERE vp.v_id = v.id and vp.p_id = $p_id";
            
            comm.Parameters.AddWithValue("$p_id", Id);

            using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(comm))
            {
                adapter.Fill(ds);
                foreach (DataRow row in ds.Tables[0].Rows)
                {

                    DateTime alku = DateTime.Parse(row["varaus_alkupvm"].ToString());
                    DateTime loppu = DateTime.Parse(row["varaus_loppupvm"].ToString());
                    int kesto = (loppu - alku).Days;
                    for (int i = 0; i < kesto; i++)
                    {
                        varatutPaivat.Add(alku.AddDays(i));
                    }
                }
            }
            conn.Close();
            return varatutPaivat;
        }

        static public Palvelu Hae(int palveluId)
        {
            Palvelu palvelu = null;
            int toimipisteid =-1;
            DataSet ds = new DataSet();
            SQLiteConnection conn = new SQLiteConnection(Properties.Settings.Default.Database);
            conn.Open();
            SQLiteCommand comm = conn.CreateCommand();
            comm.CommandText = "SELECT id, tp_id, nimi, kuvaus, hinta, alv FROM palvelu WHERE id = $id";
            comm.Parameters.AddWithValue("$id", palveluId);

            using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(comm))
            {
                adapter.Fill(ds); 

                if ( ds.Tables[0].Rows.Count > 0 )
                { 
                    DataRow row = ds.Tables[0].Rows[0];
                
                    palvelu = new Palvelu();
                    palvelu.Parsi(row);
                    toimipisteid = int.Parse(row["tp_id"].ToString());
                    
                }

            }

            conn.Close();
                        
            //palvelu.Toimipiste = Toimipiste.Hae(toimipisteid);

            return palvelu;
        }

        protected void Parsi(DataRow row )
        {
             Id = int.Parse(row["id"].ToString());
             Nimi = row["nimi"].ToString();
             Kuvaus = row["kuvaus"].ToString();
             Hinta = double.Parse(row["hinta"].ToString());
             Alv = double.Parse(row["alv"].ToString());
             
        }
    }
}

/*
 * CREATE TABLE `palvelu` (
	`id`	INTEGER PRIMARY KEY,
	`tp_id`	INTEGER NOT NULL,
	`nimi`	TEXT,
	`kuvaus`	TEXT,
	`hinta`	REAL,
	`alv`	REAL,
	FOREIGN KEY(tp_id) REFERENCES toimipiste(id) ON DELETE CASCADE
)
*/