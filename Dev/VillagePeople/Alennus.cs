﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VillagePeople.Forms
{
    class Alennus : Asiakas
    {
        private string alennuskoodi;

        public Alennus(string alennuskoodi) {
            this.alennuskoodi = alennuskoodi;
        }

        private bool nettopaivat;
        public bool Nettopaivat {
            get { return nettopaivat; }
            set { nettopaivat = value; }
        }

        private double alennuspaivatPros1;
        public double AlennuspaivatPros1 {
            get { return alennuspaivatPros1; }
            set { alennuspaivatPros1 = value; }
        }

        private double alennuspaivatPros2;
        public double AlennuspaivatPros2 {
            get { return alennuspaivatPros2; }
            set { alennuspaivatPros2 = value; }
        }
    }
}
